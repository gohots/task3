import java.util.*;

public class Main {
    static Map<String, String> map = new HashMap<String,String>();
    static Map<String, Collection<String> > newmap = new HashMap<String, Collection<String> >();
    public static void main(String[] args) {
        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", "value4");
        map.put("key4", "value4");
        map.put("key5", "value4");
        System.out.println("Начальные ключи изнвчения  \n"+map.keySet() + " " + map.values());
        reverse();
    }
    public static  void reverse(){

        for (Map.Entry<String, String> entry : map.entrySet())
        {
            String oldVal = entry.getValue();
            String oldKey = entry.getKey();
            Collection<String> newVal = null;

            if (newmap.containsKey(oldVal))
            {
                newVal = newmap.get(oldVal);
                newVal.add(oldKey);
            }
            else
            {
                newVal= new HashSet<>();
                newVal.add(oldKey);
            }
            newmap.put(oldVal, newVal);
        }
        System.out.println("Поменяные ключи и значения \n "+newmap.keySet() + " " + newmap.values());}
}
