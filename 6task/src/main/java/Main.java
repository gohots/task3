import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        Integer[][] array = new Integer[][]{{1,2,3},{4,5,6},{6,7,8}};
        ArrayIterator i = new ArrayIterator(array);
        while (i.hasNext()) {
            System.out.println(i.next());
        }

    }
    public static class ArrayIterator implements Iterator<Integer> {
        Integer[][] arr;
        int i, j;
        public ArrayIterator(Integer[][] arr) {
            this.arr = arr;
        }
        @Override
        public boolean hasNext() {
            return i < arr.length && j < arr[i].length;
        }
        @Override
        public Integer next() {
            Integer next = arr[i][j++];
            if (j >= arr[i].length) {
                i++;
                j = 0;
            }
            return next;
        }
        @Override
        public void remove() {

        }
    }
}