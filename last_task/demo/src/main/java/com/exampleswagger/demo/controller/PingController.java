package com.exampleswagger.demo.controller;


import com.exampleswagger.demo.model.PingModel;
import com.exampleswagger.demo.service.PingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class PingController {

    private final PingService pingService;

    @GetMapping("/ping")
    public PingModel ping(){
        return pingService.ping();
    }

    @PostMapping("/addMessage")
    public void add (PingModel model){
        pingService.add(model);
    }

    @GetMapping("/list")
    public List<PingModel> getList(){
        return pingService.getList();
    }

}
