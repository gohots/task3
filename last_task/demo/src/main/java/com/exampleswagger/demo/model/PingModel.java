package com.exampleswagger.demo.model;

import lombok.Data;

@Data
public class PingModel {

    private String message;
    private Integer code;
}
