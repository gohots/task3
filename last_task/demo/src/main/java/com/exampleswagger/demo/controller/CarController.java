package com.exampleswagger.demo.controller;


import com.exampleswagger.demo.model.CarModel;
import com.exampleswagger.demo.service.CarService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CarController {
    private final CarService carService;

    public CarController(CarService carService){this.carService=carService;}

    @GetMapping("/doMdel")
    public CarModel doModel(){return carService.doModel()}

}
