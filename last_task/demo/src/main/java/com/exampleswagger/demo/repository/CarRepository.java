package com.exampleswagger.demo.repository;


import com.exampleswagger.demo.model.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends  JpaRepository<CarModel>, Integer, JpaSpecificationExecutor<CarModel> {


}
