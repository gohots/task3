import java.util.Random;
import java.util.Scanner;

public class Operations {
    Vector vector = new Vector(3.4f, 2.6f, 4.4f);
    Float X = vector.getX();
    Float Y = vector.getY();
    Float Z = vector.getZ();
    Vector vector1 = new Vector(7.6f, 5.4f, 2.1f);
    Float X1 = vector1.getX();
    Float Y1 = vector1.getY();
    Float Z1 = vector1.getZ();

    public void OutputVector() {
        System.out.println("Координаты первого вектора - x =" + X + " y = " + Y + " z = " + Z);
    }

    public void OutputVector1() {
        System.out.println("Координаты второго вектора - x = " + X1 + " y = " + Y1 + " z = " + Z1);
    }

    public void VectorLength() {
        double Lenght;
        Lenght = Math.sqrt(X * X + Y * Y + Z * Z);
        System.out.println("Длина первого вектора равна - " + Lenght);
    }

    public void VectorLength1() {
        double Lenght;
        Lenght = Math.abs(Math.sqrt(X1 * X1 + Y1 * Y1 + Z1 * Z1));
        System.out.println("Длина второго вектора равна - " + Lenght);
    }

    public void Scalar() {
        Float scalar = X1 * X + Y1 * Y + Z1 * Z;
        System.out.println("Скалярное произведение является равным - " + scalar);
    }

    public void Multiply() {
        String multiply = (Y * Z1 - Z * Y1) + "*i" + "+" + (Z * X1 - X * Z1) + "*j" + "+" + (X * Y1 - Y * X1) + "*k";
        System.out.println("Векторное произведение является равным - " + multiply);
    }

    public void Cos() {
        double Lenght = Math.abs(Math.sqrt(X * X + Y * Y + Z * Z));
        double Lenght1 = Math.abs(Math.sqrt(X1 * X1 + Y1 * Y1 + Z1 * Z1));
        Float scalar = X1 * X + Y1 * Y + Z1 * Z;
        double cos = scalar / (Lenght * Lenght1);
        System.out.println("Косинус угла между векторами равен - " + cos);
    }

    public void Plus() {
        Float sum1 = X + X1;
        Float sum2 = Y + Y1;
        Float sum3 = Z + Z1;
        System.out.println("Сумма векторов равна - x:" + sum1 + "; y:" + sum2 + "; z:" + sum3);
    }

    public void Minus() {
        Float sum1 = X - X1;
        Float sum2 = Y - Y1;
        Float sum3 = Z - Z1;
        System.out.println("Сумма векторов равна - x:" + sum1 + "; y:" + sum2 + "; z:" + sum3);
    }

    public static void RandomVector() {
        System.out.println("");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество векторов которое хотите задать");
        int n = scanner.nextInt();
        int[][] a = new int[n][3];
        Random rnd = new Random();
        System.out.println("X  Y  Z");
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = rnd.nextInt(100) + 1;
            }
        }
        for (int i = 0; i < a.length; i++, System.out.println()) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
        }
    }
}

final class Vector {
    private Float x;
    private Float y;
    private Float z;

    public Vector(Float x, Float y, Float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Float getX() {
        return x;
    }

    public Float getY() {
        return y;
    }

    public Float getZ() {
        return z;
    }
}


